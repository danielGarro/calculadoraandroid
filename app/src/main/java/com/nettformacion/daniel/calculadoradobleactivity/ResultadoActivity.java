package com.nettformacion.daniel.calculadoradobleactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ResultadoActivity extends AppCompatActivity {
        EditText resultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operadores);

        Intent i = getIntent();
        Bundle res = i.getFloatExtra("txtResultado");
        resultado = (EditText) findViewById(R.id.txtResultado);
        resultado.setText = (String) res;
    }
}