package com.nettformacion.daniel.calculadoradobleactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.lang.*;//importo librería de la que extiende Toast

public class OperadoresActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operadores);

        final Button calcular = findViewById(R.id.botonCalcular);
        calcular.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /*Al hacer click en el botón:
- programamos la instancia Toast que nos muestra un mensaje de calculando... en la etiqueta del edittext donde se mostrar el resultado en la sgunda activity.
- También obtenemos los valores de los edittext
de la actividad principal correspondientes a los operadores, los casteamos a float y los sumamos.
- Finalmente el resultado se manda en un intent.putExtra a la segunda activity.
https://developer.android.com/reference/android/content/Intent#public-constructors
*/
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.resultadoActivity,
                        (ViewGroup) findViewById(R.id.resultado_container));

                TextView text = (TextView) layout.findViewById(R.id.lblResultado);
                text.setText("Calculando...");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();

                EditText op1 = (EditText) layout.findViewById(R.id.txtOperador1);
                EditText op2 = (EditText) layout.findViewById(R.id.txtOperador2);
                float o1 = (float) op1.getText();
                float o2 = (float) op2.getText();
                float resultado = o1 + o2;

                Intent i = new Intent(OperadoresActivity.this, ResultadoActivity.class);
                i.putExtra("txtResultado", resultado);
            }
        });
    }
}
